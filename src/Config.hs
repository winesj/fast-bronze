module Config where

import Data.Default qualified as D
import System.FD qualified as FD

newtype Config = Config
    { fdArgs :: [FD.Params]
    }

-- jacksTestConfig = Config [
--   D.default
--                          ]
