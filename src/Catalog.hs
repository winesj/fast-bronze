module Catalog where

import Config qualified as C
import Control.Monad.IO.Class qualified as M
import Data.Default qualified as D
import Data.List qualified as L
import Data.Map.Strict qualified as M
import Data.Maybe qualified as My
import Data.Ord (comparing)
import Data.Text qualified as T
import Shelly qualified as S
import System.Directory qualified as D
import System.FD qualified as FD
import System.Info qualified as In
import System.Posix qualified as P

data SearchDir = SearchDir
    { dirDepth :: Word
    , searchDir :: T.Text
    }
    deriving (Show, Eq, Ord)

data Action = Action
    { actionSearchString :: T.Text
    , ioAction :: IOAction
    , canRun :: Cataloged -> Bool
    }

data IOAction
    = OneParam (Cataloged -> IO ())
    | TwoParam
        { run :: Cataloged -> Cataloged -> IO ()
        , catalogStart :: Cataloged -> IO [Cataloged]
        , modFDParams :: FD.Params -> FD.Params
        }

newtype Cataloged = Cataloged
    { dirPath :: T.Text
    }
    deriving (Show, Eq, Ord)

instance Searchable Action where
    searchString = actionSearchString

instance Searchable Cataloged where
    searchString = T.toLower . T.reverse . T.takeWhile (/= '/') . T.reverse . removeTrailingSlash . dirPath

instance Show Action where
    show = T.unpack . searchString

instance Ord Action where
    compare = comparing actionSearchString

instance Eq Action where
    (==) a b = (== EQ) $ compare a b

instance Searchable T.Text where
    searchString = id

class Searchable s where
    searchString :: s -> T.Text

removeTrailingSlash :: T.Text -> T.Text
removeTrailingSlash txt = if T.last txt == '/' then T.init txt else txt

parentDir :: T.Text -> T.Text
parentDir = My.maybe "/" (T.reverse . T.dropWhile (/= '/') . snd) . T.uncons . T.reverse . removeTrailingSlash

getCatalog :: IO [Cataloged]
getCatalog = do
    absHomeDir <- D.getHomeDirectory
    searchCatalogItem
        SearchDir
            { dirDepth = 1
            , searchDir = T.pack absHomeDir
            }
        ""

homeSearchDir = do
    absHomeDir <- D.getHomeDirectory
    pure
        SearchDir
            { dirDepth = 1
            , searchDir = T.pack absHomeDir
            }

score :: T.Text -> T.Text -> Word
score query candidate = L.sum . M.elems . M.unionWith (-) (toMultiSet candidate) $ toMultiSet query
  where
    toMultiSet :: T.Text -> M.Map Char Word
    toMultiSet = M.fromListWith (+) . map (,1) . T.unpack

toFDParams :: SearchDir -> T.Text -> FD.Params
toFDParams (SearchDir{..}) query = D.def{FD.regexPattern = fuzzyRegexQuery, FD.path = searchDir, FD.depth = Just dirDepth}
  where
    fuzzyRegexQuery :: T.Text
    fuzzyRegexQuery = T.intercalate ".*?" . map T.singleton . T.unpack $ query

searchInitialCatalog :: C.Config -> IO [Cataloged]
searchInitialCatalog = fmap (map Cataloged . concat) . mapM FD.search . C.fdArgs

searchCatalogByFDParams :: FD.Params -> T.Text -> IO [Cataloged]
searchCatalogByFDParams params query =
    fmap
        ( map Cataloged
            . L.sortOn (score query)
        )
        . FD.search
        $ params

searchCatalogItem :: SearchDir -> T.Text -> IO [Cataloged]
searchCatalogItem params query = searchCatalogByFDParams (toFDParams params query) query

getActions :: Cataloged -> [Action]
getActions cataloged =
    [ Action
        { actionSearchString = "move to trash"
        , ioAction = OneParam $ \cataloged -> P.executeFile "mv" True [T.unpack . dirPath $ cataloged, "~/.Trash"] Nothing
        , canRun = const True
        }
    , Action
        { actionSearchString = "move to"
        , canRun = const True
        , ioAction =
            TwoParam
                { run = \cataloged cataloged' -> P.executeFile "mv" True (map (T.unpack . dirPath) [cataloged, cataloged']) Nothing
                , catalogStart = \firstCatalogItem -> searchCatalogItem (SearchDir 1 (parentDir . dirPath $ firstCatalogItem)) ""
                , modFDParams = \fd -> fd{FD.fileTypes = [FD.Directory]}
                }
        }
    ]
        ++ case In.os of
            "darwin" -> darwinActions
            "linux" -> linuxActions

darwinActions :: [Action]
darwinActions =
    [ Action
        { actionSearchString = "open"
        , ioAction = OneParam $ \cataloged -> P.executeFile "open" True [T.unpack . dirPath $ cataloged] Nothing
        , canRun = const True
        }
    ]

linuxActions :: [Action]
linuxActions =
    [ Action
        { actionSearchString = "xfg-open"
        , ioAction = OneParam $ \cataloged -> P.executeFile "xfg-open" True [T.unpack . dirPath $ cataloged] Nothing
        , canRun = const True
        }
    ]
