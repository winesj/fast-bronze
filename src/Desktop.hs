module Desktop where

import Data.Text qualified as T
import System.Directory qualified as D
import System.Environment.XDG.DesktopEntry qualified as DE
import System.FD qualified as FD
import System.Posix.Env qualified as P

-- getXDGDataDirs :: IO [T.Text]
-- getXDGDataDirs = T.splitOn ":" . T.pack <$> P.getEnvDefault "" "XDG_DATA_DIRS"

-- getDesktopFiles :: T.Text -> IO [T.Text]
-- getDesktopFiles dir = fmap (map T.pack) . D.listDirectory . T.unpack . T.append dir $ "/applications"
