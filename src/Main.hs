module Main where

import Brick qualified as B
import Brick.Widgets.Border qualified as BWB
import Catalog
import Control.Applicative qualified as A
import Control.Concurrent.Async (async)
import Control.Monad (void)
import Control.Monad.IO.Class
import Control.Monad.IO.Class qualified as M
import Data.ByteString qualified as BS
import Data.Char qualified as C
import Data.Function ((&))
import Data.List as L (singleton)
import Data.Maybe
import Data.Maybe qualified as My
import Data.Text (Text)
import Data.Text qualified as T
import Data.Text.IO qualified as TIO
import Data.Vector qualified as Vec
import Data.Vector.Storable.Mutable (grow)
import Debug.Trace qualified as T
import Graphics.Vty.Attributes qualified as B
import Graphics.Vty.Input qualified as Vty
import ListZipper qualified as LZ

-- import Pipes
-- import qualified Pipes.Extras as Pipes
import System.Directory qualified as D
import System.IO
import System.Posix qualified as P
import System.Posix.Files qualified as F

data Event = Search Text | Delete | Tab | Enter | Up | Down | Left | Right | ReplaceCatalog [Cataloged]

data Model = Model
    { selections :: Selections
    , searchText :: T.Text
    }
    deriving (Show, Ord, Eq)

firstCatalog :: Selections -> Maybe (LZ.ListZipper Cataloged)
firstCatalog = fmap fst

actions :: Selections -> Maybe (LZ.ListZipper Action)
actions (Just (_, Just (x, _))) = Just x
actions _ = Nothing

secondCatalog :: Selections -> Maybe (LZ.ListZipper Cataloged)
secondCatalog (Just (_, Just (_, Just x))) = Just x
secondCatalog _ = Nothing

type Selections = Maybe (LZ.ListZipper Cataloged, Maybe (LZ.ListZipper Action, Maybe (LZ.ListZipper Cataloged)))

modifySelection :: (forall s. (Searchable s) => LZ.ListZipper s -> Maybe (LZ.ListZipper s)) -> Model -> Model
modifySelection f Model{..} = Model{selections = newSelections, ..}
  where
    newSelections :: Selections
    newSelections = case selections of
        Nothing -> firstOfTuple $ (f . fst =<< selections)
        (Just (a, Nothing)) -> firstOfTuple . f $ a
        (Just (a, (Just (b, Nothing)))) -> Just (a, firstOfTuple . f $ b)
        ((Just (a, (Just (b, Just c))))) -> Just (a, (Just (b, f $ c)))

    firstOfTuple (Just x) = Just (x, Nothing)
    firstOfTuple Nothing = Nothing

currSelection :: Selections -> Maybe (LZ.ListZipper T.Text)
currSelection Nothing = Nothing
currSelection (Just (x, Nothing)) = Just . LZ.map searchString $ x
currSelection (Just (_, (Just (x, Nothing)))) = Just . LZ.map searchString $ x
currSelection ((Just (_, (Just (_, Just x))))) = Just . LZ.map searchString $ x

data Focus = Catalog | Actions | SecondCatalog deriving (Show, Ord, Eq)
justPrefer :: Maybe a -> Maybe a -> Maybe a
justPrefer (Just a) _ = Just a
justPrefer _ a = a

draw (Model{..}) =
    L.singleton $
        B.vBox
            [ B.txt searchText
            , BWB.border (B.txt $ maybe "type to enter text" (dirPath . LZ.focus) (firstCatalog selections))
            , BWB.border (B.txt $ maybe "empty actions selection" (searchString . LZ.focus) (actions selections))
            , maybe B.emptyWidget (BWB.border . B.vBox . toPrettyList . LZ.map B.txt) (currSelection selections)
            ]

toPrettyList LZ.ListZipper{..} = (reverse . take beforeTakeLen $ before) ++ (BWB.border focus) : (take afterTakeLen after)
  where
    -- if one side doesn't have 5 items, we want the other to pick up the slack
    beforeTakeLen = 5 + max 0 (5 - length after)
    afterTakeLen = 5 + max 0 (5 - length before)

handleEvent :: B.BrickEvent n1 e -> B.EventM n2 Model ()
handleEvent (B.VtyEvent (Vty.EvKey (Vty.KChar 'c') [Vty.MCtrl])) = B.halt
handleEvent (B.VtyEvent (Vty.EvKey Vty.KUp [])) = B.modify $ modifySelection (Just . LZ.moveRight)
handleEvent (B.VtyEvent (Vty.EvKey Vty.KDown [])) = B.modify $ modifySelection (Just . LZ.moveLeft)
handleEvent (B.VtyEvent (Vty.EvKey Vty.KLeft [])) = do
    Model{..} <- B.get
    newCatalog' <- liftIO $ mapM (flip searchCatalogItem searchText . SearchDir 1 . parentDir . parentDir . dirPath . LZ.focus . fst) selections
    B.put Model{selections = fmap (,Nothing) . LZ.fromList =<< newCatalog', searchText = ""}
handleEvent (B.VtyEvent (Vty.EvKey Vty.KRight [])) = do
    Model{..} <- B.get
    newCatalog' <- liftIO $ mapM (flip searchCatalogItem "" . SearchDir 1 . dirPath . LZ.focus . fst) selections
    let newSelections = (case LZ.fromList =<< newCatalog' of Nothing -> fmap fst selections; a -> a)
    B.put Model{selections = fmap (,Nothing) newSelections, searchText = "", ..}
handleEvent (B.VtyEvent (Vty.EvKey Vty.KBS [])) = do
    Model{..} <- B.get
    let newSearchText = maybe "" fst $ T.unsnoc searchText
    newModel <- case firstCatalog selections of
        Just firstCatalog' -> do
            newCatalog <- M.liftIO . searchCatalogItem (SearchDir 1 . parentDir . dirPath . LZ.focus $ firstCatalog') $ newSearchText
            pure
                Model
                    { selections = (,Nothing) <$> (flip LZ.focusOn (LZ.focus firstCatalog') <$> LZ.fromList newCatalog)
                    , searchText = newSearchText
                    }
        Nothing -> pure . searchFor $ Model{searchText = newSearchText, ..}
    B.put newModel
handleEvent (B.VtyEvent (Vty.EvKey (Vty.KChar '\t') [])) = do
    model <- B.get
    newSelections <- liftIO $ nextSelections (selections model)
    B.put $ searchFor $ model{selections = newSelections, searchText = ""}
handleEvent (B.VtyEvent (Vty.EvKey Vty.KEnter [])) = do
    model <- B.get
    case selections model of
        (Just (LZ.ListZipper{focus = catalogFocus}, Just (LZ.ListZipper{focus = (Action{ioAction = OneParam run})}, Nothing))) -> liftIO $ run catalogFocus
        (Just (LZ.ListZipper{focus = catalogFocus}, Just (LZ.ListZipper{focus = (Action{ioAction = (TwoParam{..})})}, Just (LZ.ListZipper{focus = catalogFocusTwo})))) -> liftIO $ run catalogFocus catalogFocusTwo
        _ -> pure ()
    B.put model
handleEvent (B.VtyEvent (Vty.EvKey (Vty.KChar inputChar) [])) = do
    liftIO . TIO.appendFile "./input.txt" . T.singleton $ inputChar
    model <- B.get
    B.put $ searchFor $ model{searchText = T.snoc (searchText model) inputChar}
handleEvent e = do
    B.continueWithoutRedraw

attrMap _ = B.attrMap B.defAttr []

nextSelections :: Selections -> IO Selections
nextSelections (Just (catalog, Nothing)) = pure . Just $ (catalog, (,Nothing) <$> (LZ.fromList . getActions $ LZ.focus catalog))
nextSelections (Just (catalog, Just (actions, Nothing))) =
    case (LZ.focus actions) of
        (Action{ioAction = (OneParam _)}) -> pure . Just $ (catalog, Just (actions, Nothing))
        (Action{ioAction = (TwoParam{..})}) -> do
            newSecondCatalog <- catalogStart . LZ.focus $ catalog
            pure . Just $ (catalog, Just (actions, LZ.fromList newSecondCatalog))
nextSelections a = pure a

searchFor :: Model -> Model
searchFor model = modifySelection (LZ.filter fuzzyMatchSearchable) model
  where
    fuzzyMatchSearchable :: (Searchable s) => s -> Bool
    fuzzyMatchSearchable s = fuzzyMatches (searchText model) (searchString s)

tuiApp :: B.App Model Event T.Text
tuiApp =
    B.App
        { appDraw = draw
        , appChooseCursor = const . const $ Nothing
        , appHandleEvent = handleEvent
        , appStartEvent = B.continueWithoutRedraw
        , appAttrMap = attrMap
        }

main :: IO ()
main = do
    currDir <- homeSearchDir
    initialSelections <- searchCatalogItem currDir ""
    B.defaultMain tuiApp $
        Model
            { searchText = ""
            , selections = (,Nothing) <$> LZ.fromList initialSelections
            }
    pure ()

fuzzyMatches :: T.Text -> T.Text -> Bool
fuzzyMatches "" _ = True
fuzzyMatches _ "" = False
fuzzyMatches s s'
    | T.head s == T.head s' = fuzzyMatches (T.tail s) (T.tail s')
    | otherwise = fuzzyMatches s (T.tail s')
