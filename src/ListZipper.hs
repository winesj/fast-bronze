module ListZipper where

import Data.Foldable qualified as F
import Data.Maybe qualified as My

data ListZipper a = ListZipper
    { before :: [a]
    , focus :: a
    , after :: [a]
    }
    deriving (Show, Ord, Eq)

moveLeft :: ListZipper a -> ListZipper a
moveLeft (ListZipper xs x []) = ListZipper xs x []
moveLeft (ListZipper xs x xs') = ListZipper (x : xs) (head xs') (tail xs')

moveRight :: ListZipper a -> ListZipper a
moveRight (ListZipper [] x xs) = ListZipper [] x xs
moveRight (ListZipper xs x xs') = ListZipper (tail xs) (head xs) (x : xs')

fromList :: [a] -> Maybe (ListZipper a)
fromList [] = Nothing
fromList (x : xs) = Just $ ListZipper [] x xs

map :: (a -> b) -> ListZipper a -> ListZipper b
map f (ListZipper xs x xs') = ListZipper (Prelude.map f xs) (f x) (Prelude.map f xs')

mapFocus :: (a -> a) -> ListZipper a -> ListZipper a
mapFocus f (ListZipper xs x xs') = ListZipper xs (f x) xs'

asList :: ListZipper a -> [a]
asList (ListZipper xs x xs') = reverse xs ++ x : xs'

asListMaybe :: Maybe (ListZipper a) -> [a]
asListMaybe Nothing = []
asListMaybe (Just a) = asList a

filter :: (a -> Bool) -> ListZipper a -> Maybe (ListZipper a)
filter p (ListZipper xs x xs') = case (Prelude.filter p xs, justIf p x, Prelude.filter p xs') of
    ([], Nothing, []) -> Nothing
    (x : xs, Nothing, xs') -> Just $ ListZipper xs x xs'
    (xs, Nothing, x : xs') -> Just $ ListZipper xs x xs'
    (xs, Just x, xs') -> Just $ ListZipper xs x xs'

justIf :: (a -> Bool) -> a -> Maybe a
justIf fn x = if fn x then Just x else Nothing

length (ListZipper{..}) = F.length before + 1 + F.length after

focusOn :: (Eq a) => ListZipper a -> a -> ListZipper a
focusOn lz seek = My.fromMaybe lz ((F.find ((== seek) . focus) . takeLzLen . iterate moveRight) =<< farthestLeft)
  where
    farthestLeft = F.find (null . before) . takeLzLen . iterate moveLeft $ lz

    takeLzLen = take (ListZipper.length lz)
