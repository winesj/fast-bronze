{-
  This could be a completely seperate package really
  It's a haskell interface to FD
-}
module System.FD where

import Data.Default qualified as Data
import Data.List qualified as L
import Data.Maybe qualified as My
import Data.Text qualified as T
import Shelly qualified as S

data Params = Params
    { regexPattern :: T.Text
    , path :: T.Text
    , depth :: Maybe Word
    , fileTypes :: [FileType]
    , extension :: Maybe T.Text
    , exclude :: Maybe T.Text
    , threads :: Maybe Word
    }
    deriving (Eq, Ord, Show)

data FileType = File | Directory | SymLink | Socket | Pipe | Executable | Empty deriving (Eq, Ord, Show)

instance Data.Default Params where
    def :: Params
    def =
        Params
            { regexPattern = ""
            , path = "."
            , depth = Nothing
            , fileTypes = []
            , extension = Nothing
            , exclude = Nothing
            , threads = Nothing
            }

fileTypeToArg :: FileType -> [T.Text]
fileTypeToArg x = ("--type" :) . L.singleton $ case x of
    File -> "f"
    Directory -> "d"
    SymLink -> "l"
    Socket -> "s"
    Pipe -> "p"
    Executable -> "x"
    Empty -> "e"

depthToArg :: Maybe Word -> [T.Text]
depthToArg Nothing = []
depthToArg (Just n) = ["--maxdepth", T.pack . show $ n]

extensionToArg :: Maybe T.Text -> [T.Text]
extensionToArg Nothing = []
extensionToArg (Just txt) = ["--extension", T.pack . show $ txt]

excludeToArg :: Maybe T.Text -> [T.Text]
excludeToArg Nothing = []
excludeToArg (Just txt) = ["--exclude", T.pack . show $ txt]

threadsToArg :: Maybe Word -> [T.Text]
threadsToArg Nothing = []
threadsToArg (Just n) = ["--threads", T.pack . show $ n]

search :: Params -> IO [T.Text]
search (Params{..}) =
    fmap T.lines
        . S.shelly
        . S.handleany_sh (const $ pure "")
        . S.silently
        . S.run "fd"
        $ depthToArg depth
            ++ concatMap fileTypeToArg fileTypes
            ++ extensionToArg extension
            ++ excludeToArg exclude
            ++ threadsToArg threads
            ++ [regexPattern, path]
