# fast-bronze
a [quicksilver](https://qsapp.com) clone

## to run (nix)
* `nix run`

## to run (other)
* install [haskell](https://www.haskell.org/ghcup/) tooling if you don't have it
* `cabal run`
